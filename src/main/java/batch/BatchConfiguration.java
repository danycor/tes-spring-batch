package batch;

import batch.entities.Client;
import batch.entities.MonthPayment;
import batch.processor.MonthPaymentProcessor;
import batch.processor.MyItemReader;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;


@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public HibernateConfiguration hibernateConfiguration;

    @Bean
    public String getInitializeSchema() {
        return "always";
    }


    @Bean
    public ItemReader<Client> reader() {
        return new MyItemReader(1, hibernateConfiguration.getSessionFactory());
    }


    @Bean
    public MonthPaymentProcessor processor() {
        return new MonthPaymentProcessor();
    }

    @Bean
    public CalculationItemWriter writer() {
        return new CalculationItemWriter(hibernateConfiguration.getSessionFactory());
    }

    @Bean
    public Job importUserJob(Step step1) {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(CalculationItemWriter writer) {
        return stepBuilderFactory.get("step1")
                .<Client, List<MonthPayment>>chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(writer)
                .build();
    }

}
