package batch;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

@Component
public class CalculationJobListener extends JobExecutionListenerSupport {
    @Override
    public void afterJob(JobExecution jobExecution) {

        System.out.println("Job " + jobExecution.getJobConfigurationName() + " ending :" + jobExecution.getCreateTime() + " to " + jobExecution.getEndTime());
        super.afterJob(jobExecution);
    }
}
