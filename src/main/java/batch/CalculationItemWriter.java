package batch;

import batch.entities.MonthPayment;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class CalculationItemWriter implements ItemWriter<List<MonthPayment>> {

    private final SessionFactory sessionFactory;

    public CalculationItemWriter(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void write(List items) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        for (List<MonthPayment> monthPaymentList : ((List<List<MonthPayment>>) items)) {
            for (Object monthPayment : monthPaymentList) {
                session.save(monthPayment);
            }
        }
        session.getTransaction().commit();
        session.close();
    }

}
