package batch.processor;

import batch.entities.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.batch.item.ItemReader;

import java.util.List;


public class MyItemReader implements ItemReader<Client> {
    private final SessionFactory sessionFactory;
    private final int batchSize;
    private List<Client> batch;
    private int currentBatch = 0;
    private int currentBatchPosition;

    private int readCount = 0;


    public MyItemReader(int batchSize, SessionFactory sessionFactory) {
        this.batchSize = batchSize;
        this.sessionFactory = sessionFactory;
        this.initBatch();
    }

    @Override
    public Client read() throws Exception {
        System.out.println("Read count : " + (++readCount));
        if (batch == null || currentBatchPosition >= batch.size()) {
            ++this.currentBatch;
            this.initBatch();
        }
        Client client = null;
        if (this.batch.size() > currentBatchPosition) {
            client = this.batch.get(currentBatchPosition);
            ++currentBatchPosition;
        }
        return client;
    }

    private void initBatch() {
        Session session = sessionFactory.openSession();
        Query<Client> query = session.createQuery("from Client as client inner join fetch client.contracts");
        query.setFirstResult(this.currentBatch * this.batchSize);
        query.setMaxResults(this.batchSize);
        this.batch = query.list();
        this.currentBatchPosition = 0;
        session.close();
    }
}
