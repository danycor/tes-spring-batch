package batch.processor;

import batch.entities.Client;
import batch.entities.Contract;
import batch.entities.MonthPayment;
import org.springframework.batch.item.ItemProcessor;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class MonthPaymentProcessor implements ItemProcessor<Client, List<MonthPayment>> {

    @Override
    public List<MonthPayment> process(Client client) {

        LocalDate endDate = LocalDate.now().plus(2, ChronoUnit.MONTHS);

        List<MonthPayment> monthPayments = new ArrayList<>();
        for (LocalDate startDate = this.getStartDate(client); startDate.compareTo(endDate) < 0; startDate = startDate.plus(1, ChronoUnit.MONTHS)) {
            MonthPayment monthPayment = new MonthPayment();
            monthPayment.setClient(client);
            monthPayment.setMonth(Date.from(startDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
            monthPayment.setAmount(this.getTotalAmount(client, startDate));
            monthPayments.add(monthPayment);
        }

        return monthPayments;
    }


    protected LocalDate getStartDate(Client client) {
        LocalDate startDate = new Date(Long.MAX_VALUE).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        for (Iterator<Contract> it = client.getContracts().stream().iterator(); it.hasNext(); ) {
            Contract contract = it.next();
            if (startDate.compareTo(contract.getCreation()) > 0) {
                startDate = contract.getCreation();
            }
        }
        return startDate;
    }

    protected float getTotalAmount(Client client, LocalDate currentDate) {
        float totalAmount = 0;
        for (Iterator<Contract> it = client.getContracts().stream().iterator(); it.hasNext(); ) {
            Contract contract = it.next();
            if (contract.getCreation().compareTo(currentDate) < 0) {
                totalAmount += contract.getPrice();
            }
        }
        return totalAmount;
    }
}
