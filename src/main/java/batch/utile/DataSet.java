package batch.utile;

import batch.entities.Client;
import batch.entities.Contract;
import com.github.javafaker.Faker;
import org.hibernate.Session;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

public class DataSet {

    public void makeDataSet(Session session, int clientCount, Range contractRange, Range priceRange) {
        session.beginTransaction();
        for (int j = 0; j < clientCount; ++j) {
            makeClient(session, contractRange, priceRange);
        }
        session.getTransaction().commit();
        session.close();
    }

    protected void makeClient(Session session, Range contractRange, Range priceRange) {
        Faker faker = new Faker();
        Client client = new Client(faker.name().firstName(), faker.name().lastName(), null);
        session.save(client);

        int nbContract = contractRange.rand();
        for (int i = 0; i < nbContract; ++i) {
            Contract contract = new Contract(
                    faker.code().isbn13(),
                    faker.date().past(60, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                    priceRange.rand(),
                    client);
            session.save(contract);
        }
    }

}
