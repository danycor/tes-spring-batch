package batch.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Clients")
public class Client {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Long id;

    @Column(name = "first_name")
    private String firstName = "";

    @Column(name = "last_name")
    private String lastName = "";

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client")
    private Set<Contract> contracts;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client")
    private Set<MonthPayment> monthPayments;


    public Client(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Client(String firstName, String lastName, Set<Contract> contracts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.contracts = contracts;
    }

    public Client() {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return this.firstName.toLowerCase() + " " + this.lastName.toUpperCase();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(Set<Contract> contracts) {
        this.contracts = contracts;
    }

    public Set<MonthPayment> getMonthPayments() {
        return monthPayments;
    }

    public void setMonthPayments(Set<MonthPayment> monthPayments) {
        this.monthPayments = monthPayments;
    }
}
