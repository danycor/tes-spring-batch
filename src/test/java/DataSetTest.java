import batch.HibernateConfiguration;
import batch.utile.DataSet;
import batch.utile.Range;
import junit.framework.TestCase;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class DataSetTest extends TestCase {
    private HibernateConfiguration hibernateConfiguration;

    @Override
    protected void setUp() {
        hibernateConfiguration = new HibernateConfiguration();
    }

    @Override
    protected void tearDown() {
        if (hibernateConfiguration.getSessionFactory() != null) {
            hibernateConfiguration.getSessionFactory().close();
        }
    }

    public void testCreateBigDataSet() {
        SessionFactory sessionFactory = hibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        new DataSet().makeDataSet(session, 200, new Range(1, 4), new Range(10, 100));

        session = sessionFactory.openSession();
        session.beginTransaction();
        List resultClients = session.createQuery("from Client").list();
        List resultContracts = session.createQuery("from Contract").list();
        assertEquals(200, resultClients.size());
        assertTrue(resultContracts.size() >= 200);
        session.getTransaction().commit();
        session.close();
    }

}
