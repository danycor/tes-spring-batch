import batch.HibernateConfiguration;
import batch.Main;
import batch.utile.DataSet;
import batch.utile.Range;
import junit.framework.TestCase;
import org.hibernate.Session;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.*;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBatchTest
@SpringBootTest(classes = Main.class)
public class BatchTest extends TestCase {

    @Autowired
    private HibernateConfiguration hibernateConfiguration;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Override
    protected void setUp() {
    }

    @Override
    protected void tearDown() {
        if (hibernateConfiguration.getSessionFactory() != null) {
            hibernateConfiguration.getSessionFactory().close();
        }
    }

    @Test
    public void launchJob() throws Exception {
        int nbClient = 200;
        Session session = hibernateConfiguration.getSessionFactory().openSession();
        new DataSet().makeDataSet(session, nbClient, new Range(1, 4), new Range(10, 100));
        // Job parameters
        Map<String, JobParameter> jobParametersMap = new HashMap<>();
        jobParametersMap.put("time", new JobParameter(System.currentTimeMillis()));

        // launch the job
        JobExecution jobExecution = jobLauncherTestUtils.launchJob(new JobParameters(jobParametersMap));

        // assert job run status
        assertEquals(BatchStatus.COMPLETED, jobExecution.getStatus());

        // output step summaries
        /*
          TODO : it's fail because MyItemReader.read is call before jobLauncherTestUtils.launchJob.
          I dont know why
         */
        for (StepExecution step : jobExecution.getStepExecutions()) {
            System.out.println(step.getSummary());
            assertEquals(nbClient, step.getWriteCount());
        }
    }

}
