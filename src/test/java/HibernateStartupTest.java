import batch.HibernateConfiguration;
import batch.entities.Client;
import junit.framework.TestCase;
import org.hibernate.Session;

import java.util.List;

public class HibernateStartupTest extends TestCase {
    private HibernateConfiguration hibernateConfiguration;

    @Override
    protected void setUp() {
        hibernateConfiguration = new HibernateConfiguration();
    }

    @Override
    protected void tearDown() {
        if (hibernateConfiguration.getSessionFactory() != null) {
            hibernateConfiguration.getSessionFactory().close();
        }
    }

    public void testBasicUsage() {
        Client client1 = new Client("dany", "corbineau");
        Client client2 = new Client("pierre", "chene");

        Session session = hibernateConfiguration.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(client1);
        session.save(client2);
        session.getTransaction().commit();
        session.close();

        session = hibernateConfiguration.getSessionFactory().openSession();
        session.beginTransaction();
        List result = session.createQuery("from Client").list();
        assertEquals(2, result.size());
        session.getTransaction().commit();
        session.close();
    }

}
